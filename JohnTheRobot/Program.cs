﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JohnTheRobot
{
    class Program
    {
        private static void Main(string[] args)
        {
            var john = new Humanoid(new Dancing());
            Console.WriteLine(john.ShowSkill()); //print dancing

            var alex = new Humanoid(new Cooking());
            Console.WriteLine(alex.ShowSkill());//print cooking

            var bob = new Humanoid();
            Console.WriteLine(bob.ShowSkill());//print no skill is defined
            Console.ReadLine();
        }

    }
    public class Humanoid
    {
        IPrintmessage _printmessage;
        public Humanoid(){ }
        public Humanoid(IPrintmessage printmessage)
        {
            _printmessage = printmessage;
        }
  
        public string ShowSkill()
        {
            try
            {
                return _printmessage.ShowSkill();
            }
            catch
            {
                return "no skill is defined";
            }
           
        }
    }

    #region Dancing
    public interface IPrintmessage
    {
        string ShowSkill();
    }
    public class Dancing : IPrintmessage
    {
        public string ShowSkill()
        {
            return "dancing";
        }
    }
    #endregion

    #region Cooking
   
    public class Cooking : IPrintmessage
    {
        public string ShowSkill()
        {
            return "cooking";
        }
    }
    #endregion

}
