﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace AlexaSettings
{
    class Program
    {
        private static void Main(string[] args)
        {
            var alexa = new Alexa();
            Console.WriteLine(alexa.Talk()); //print hello, i am Alexa

            alexa.Configure(x =>
            {
                x.GreetingMessage = "Hello {OwnerName}, I'm at your service";
                x.OwnerName = "Bob Marley";
            });

            Console.WriteLine(alexa.Talk()); //print Hello Bob Marley, I'm at your service

            Console.WriteLine("press any key to exit ...");
            Console.ReadKey();

        }

    }
    public class Alexa
    {
        public string GreetingMessage { get; set; }
        public string OwnerName { get; set; }
        public Alexa()
        {

        }
        public string Talk()
        {
            if (!string.IsNullOrEmpty(GreetingMessage))
                return GreetingMessage;
            return "hello, i am Alexa";
        }

        public void Configure(Action<Alexa> p)
        {
            var alexa = p;
            this.GreetingMessage = alexa.Method.Name;
            
        }
    }
}
