﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UserClass
{
    class Program
    {
        private static void Main(string[] args)
        {
            while (true)
            {
                var user = new User();
                Console.WriteLine("please enter the username, or q to exit");
                var userName = Console.ReadLine();
                if (userName == "q")
                {
                    break;
                }

                user.Add(userName);
                Console.WriteLine($"number of addedUser {user.GetUsersCount()}");
            }
        }

    }
    public class User
    {
        private static ArrayList _usersArray = new ArrayList();

        public void Add(string username)
        {
            _usersArray.Add(username);
        }

        public int GetUsersCount()
        {
            return _usersArray.Count;
        }
    }

}
